#!/usr/bin/env python
from pathlib import Path
import argparse
import os
import sys


parser = argparse.ArgumentParser()
parser.add_argument("file", help="swaps too different files")
parser.add_argument("file2")
# parser.add_argument("-d", "--directory", action="store_true",
# parser.add_argument("-n", "--nameonly", action="store_true",
#                     help="swaps the name of a file but not content")

# probably implement additional stuff later
args = parser.parse_args()


class File():
    """initalizes a file i can manipulate"""
    def __init__(self, file_path):
        self.file_path = Path(file_path)
        self.test_file()
        self.file_data = Path(file_path).read_text()

    def test_file(self):
        """checks if file exists and rw permission. if missing quits"""
        # avoids altering unusable files and losing data
        if not self.file_path.exists():
            sys.exit(f"Error: {self.file_path} no file found")
        if not self.file_path.is_file():
            sys.exit(f"Error: {self.file_path} isn't a file")
        # I wanted to use only pathlib but i couldn't figure out how to do this
        if not os.access(self.file_path, os.R_OK):
            sys.exit(f"Error: {self.file_path} no read permission")
        if not os.access(self.file_path, os.W_OK):
            sys.exit(f"Error: {self.file_path} missing write permissions")


# checks both files to ensure they are safe to use
# also adds them to a class
file_one = File(args.file)
file_two = File(args.file2)

# It looks kinda weird and long but looking at it its pretty simple here
# FILE     PATH      METHOD        DATA
file_one.file_path.write_text(file_two.file_data)
file_two.file_path.write_text(file_one.file_data)
# potentially could mess around with a ton of files lol
